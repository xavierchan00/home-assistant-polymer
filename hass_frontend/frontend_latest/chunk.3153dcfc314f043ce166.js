/*! For license information please see chunk.3153dcfc314f043ce166.js.LICENSE */
(self.webpackJsonp=self.webpackJsonp||[]).push([[42],{202:function(e,a,i){"use strict";i(4),i(71),i(164);var s=i(5),r=i(3),t=i(131);const l=r.a`
  <style include="paper-spinner-styles"></style>

  <div id="spinnerContainer" class-name="[[__computeContainerClasses(active, __coolingDown)]]" on-animationend="__reset" on-webkit-animation-end="__reset">
    <div class="spinner-layer layer-1">
      <div class="circle-clipper left">
        <div class="circle"></div>
      </div>
      <div class="circle-clipper right">
        <div class="circle"></div>
      </div>
    </div>

    <div class="spinner-layer layer-2">
      <div class="circle-clipper left">
        <div class="circle"></div>
      </div>
      <div class="circle-clipper right">
        <div class="circle"></div>
      </div>
    </div>

    <div class="spinner-layer layer-3">
      <div class="circle-clipper left">
        <div class="circle"></div>
      </div>
      <div class="circle-clipper right">
        <div class="circle"></div>
      </div>
    </div>

    <div class="spinner-layer layer-4">
      <div class="circle-clipper left">
        <div class="circle"></div>
      </div>
      <div class="circle-clipper right">
        <div class="circle"></div>
      </div>
    </div>
  </div>
`;l.setAttribute("strip-whitespace",""),Object(s.a)({_template:l,is:"paper-spinner",behaviors:[t.a]})},205:function(e,a,i){"use strict";i(160);var s=i(75),r=i(1),t=i(99);const l={getTabbableNodes:function(e){var a=[];return this._collectTabbableNodes(e,a)?t.a._sortByTabIndex(a):a},_collectTabbableNodes:function(e,a){if(e.nodeType!==Node.ELEMENT_NODE||!t.a._isVisible(e))return!1;var i,s=e,l=t.a._normalizedTabIndex(s),o=l>0;l>=0&&a.push(s),i="content"===s.localName||"slot"===s.localName?Object(r.a)(s).getDistributedNodes():Object(r.a)(s.shadowRoot||s.root||s).children;for(var d=0;d<i.length;d++)o=this._collectTabbableNodes(i[d],a)||o;return o}},o=customElements.get("paper-dialog"),d={get _focusableNodes(){return l.getTabbableNodes(this)}};customElements.define("ha-paper-dialog",class extends(Object(s.b)([d],o)){})},735:function(e,a,i){"use strict";i.r(a);i(87),i(202);var s=i(3),r=i(25),t=(i(205),i(97),i(190));customElements.define("ha-dialog-add-user",class extends(Object(t.a)(r.a)){static get template(){return s.a`
      <style include="ha-style-dialog">
        .error {
          color: red;
        }
        ha-paper-dialog {
          max-width: 500px;
        }
        .username {
          margin-top: -8px;
        }
      </style>
      <ha-paper-dialog
        id="dialog"
        with-backdrop
        opened="{{_opened}}"
        on-opened-changed="_openedChanged"
      >
        <h2>[[localize('ui.panel.config.users.add_user.caption')]]</h2>
        <div>
          <template is="dom-if" if="[[_errorMsg]]">
            <div class="error">[[_errorMsg]]</div>
          </template>
          <paper-input
            class="name"
            label="[[localize('ui.panel.config.users.add_user.name')]]"
            value="{{_name}}"
            required
            auto-validate
            autocapitalize="on"
            error-message="Required"
            on-blur="_maybePopulateUsername"
          ></paper-input>
          <paper-input
            class="username"
            label="[[localize('ui.panel.config.users.add_user.username')]]"
            value="{{_username}}"
            required
            auto-validate
            autocapitalize="none"
            error-message="Required"
          ></paper-input>
          <paper-input
            label="[[localize('ui.panel.config.users.add_user.password')]]"
            type="password"
            value="{{_password}}"
            required
            auto-validate
            error-message="Required"
          ></paper-input>
        </div>
        <div class="buttons">
          <template is="dom-if" if="[[_loading]]">
            <div class="submit-spinner">
              <paper-spinner active></paper-spinner>
            </div>
          </template>
          <template is="dom-if" if="[[!_loading]]">
            <mwc-button on-click="_createUser"
              >[[localize('ui.panel.config.users.add_user.create')]]</mwc-button
            >
          </template>
        </div>
      </ha-paper-dialog>
    `}static get properties(){return{_hass:Object,_dialogClosedCallback:Function,_loading:{type:Boolean,value:!1},_errorMsg:String,_opened:{type:Boolean,value:!1},_name:String,_username:String,_password:String}}ready(){super.ready(),this.addEventListener("keypress",e=>{13===e.keyCode&&this._createUser(e)})}showDialog({hass:e,dialogClosedCallback:a}){this.hass=e,this._dialogClosedCallback=a,this._loading=!1,this._opened=!0,setTimeout(()=>this.shadowRoot.querySelector("paper-input").focus(),0)}_maybePopulateUsername(){if(this._username)return;const e=this._name.split(" ");e.length&&(this._username=e[0].toLowerCase())}async _createUser(e){if(e.preventDefault(),!this._name||!this._username||!this._password)return;let a;this._loading=!0,this._errorMsg=null;try{a=(await this.hass.callWS({type:"config/auth/create",name:this._name})).user.id}catch(i){return this._loading=!1,void(this._errorMsg=i.code)}try{await this.hass.callWS({type:"config/auth_provider/homeassistant/create",user_id:a,username:this._username,password:this._password})}catch(i){return this._loading=!1,this._errorMsg=i.code,void(await this.hass.callWS({type:"config/auth/delete",user_id:a}))}this._dialogDone(a)}_dialogDone(e){this._dialogClosedCallback({userId:e}),this.setProperties({_errorMsg:null,_username:"",_password:"",_dialogClosedCallback:null,_opened:!1})}_equals(e,a){return e===a}_openedChanged(e){this._dialogClosedCallback&&!e.detail.value&&this._dialogDone()}})}}]);
//# sourceMappingURL=chunk.3153dcfc314f043ce166.js.map