/*! For license information please see chunk.6ba92771b277161ae670.js.LICENSE */
(self.webpackJsonp=self.webpackJsonp||[]).push([[151],{205:function(e,t,l){"use strict";l(160);var s=l(75),o=l(1),a=l(99);const i={getTabbableNodes:function(e){var t=[];return this._collectTabbableNodes(e,t)?a.a._sortByTabIndex(t):t},_collectTabbableNodes:function(e,t){if(e.nodeType!==Node.ELEMENT_NODE||!a.a._isVisible(e))return!1;var l,s=e,i=a.a._normalizedTabIndex(s),r=i>0;i>=0&&t.push(s),l="content"===s.localName||"slot"===s.localName?Object(o.a)(s).getDistributedNodes():Object(o.a)(s.shadowRoot||s.root||s).children;for(var n=0;n<l.length;n++)r=this._collectTabbableNodes(l[n],t)||r;return r}},r=customElements.get("paper-dialog"),n={get _focusableNodes(){return i.getTabbableNodes(this)}};customElements.define("ha-paper-dialog",class extends(Object(s.b)([n],r)){})},216:function(e,t,l){"use strict";l(4),l(43),l(42);var s=l(132),o=l(5),a=l(3);Object(o.a)({_template:a.a`
    <style>

      :host {
        display: block;
        @apply --layout-relative;
      }

      :host(.is-scrolled:not(:first-child))::before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        height: 1px;
        background: var(--divider-color);
      }

      :host(.can-scroll:not(.scrolled-to-bottom):not(:last-child))::after {
        content: '';
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        height: 1px;
        background: var(--divider-color);
      }

      .scrollable {
        padding: 0 24px;

        @apply --layout-scroll;
        @apply --paper-dialog-scrollable;
      }

      .fit {
        @apply --layout-fit;
      }
    </style>

    <div id="scrollable" class="scrollable" on-scroll="updateScrollState">
      <slot></slot>
    </div>
`,is:"paper-dialog-scrollable",properties:{dialogElement:{type:Object}},get scrollTarget(){return this.$.scrollable},ready:function(){this._ensureTarget(),this.classList.add("no-padding")},attached:function(){this._ensureTarget(),requestAnimationFrame(this.updateScrollState.bind(this))},updateScrollState:function(){this.toggleClass("is-scrolled",this.scrollTarget.scrollTop>0),this.toggleClass("can-scroll",this.scrollTarget.offsetHeight<this.scrollTarget.scrollHeight),this.toggleClass("scrolled-to-bottom",this.scrollTarget.scrollTop+this.scrollTarget.offsetHeight>=this.scrollTarget.scrollHeight)},_ensureTarget:function(){this.dialogElement=this.dialogElement||this.parentElement,this.dialogElement&&this.dialogElement.behaviors&&this.dialogElement.behaviors.indexOf(s.b)>=0?(this.dialogElement.sizingTarget=this.scrollTarget,this.scrollTarget.classList.remove("fit")):this.dialogElement&&this.scrollTarget.classList.add("fit")}})},736:function(e,t,l){"use strict";l.r(t);l(216);var s=l(3),o=l(25),a=(l(205),l(97),l(192));customElements.define("zwave-log-dialog",class extends(Object(a.a)(o.a)){static get template(){return s.a`
    <style include="ha-style-dialog">
    </style>
      <ha-paper-dialog id="pwaDialog" with-backdrop="" opened="{{_opened}}">
        <h2>OpenZwave internal logfile</h2>
        <paper-dialog-scrollable>
          <pre>[[_ozwLog]]</pre>
        <paper-dialog-scrollable>
      </ha-paper-dialog>
      `}static get properties(){return{hass:Object,_ozwLog:String,_dialogClosedCallback:Function,_opened:{type:Boolean,value:!1},_intervalId:String,_numLogLines:{type:Number}}}ready(){super.ready(),this.addEventListener("iron-overlay-closed",e=>this._dialogClosed(e))}showDialog({_ozwLog:e,hass:t,_tail:l,_numLogLines:s,dialogClosedCallback:o}){this.hass=t,this._ozwLog=e,this._opened=!0,this._dialogClosedCallback=o,this._numLogLines=s,setTimeout(()=>this.$.pwaDialog.center(),0),l&&this.setProperties({_intervalId:setInterval(()=>{this._refreshLog()},1500)})}async _refreshLog(){const e=await this.hass.callApi("GET","zwave/ozwlog?lines="+this._numLogLines);this.setProperties({_ozwLog:e})}_dialogClosed(e){if("ZWAVE-LOG-DIALOG"===e.target.nodeName){clearInterval(this._intervalId),this._opened=!1;const e=!0;this._dialogClosedCallback({closedEvent:e}),this._dialogClosedCallback=null}}})}}]);
//# sourceMappingURL=chunk.6ba92771b277161ae670.js.map