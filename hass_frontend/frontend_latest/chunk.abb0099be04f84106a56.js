(self.webpackJsonp=self.webpackJsonp||[]).push([[93],{190:function(t,e,i){"use strict";var r=i(8);e.a=Object(r.a)(t=>(class extends t{static get properties(){return{hass:Object,localize:{type:Function,computed:"__computeLocalize(hass.localize)"}}}__computeLocalize(t){return t}}))},191:function(t,e,i){"use strict";var r=i(0);function s(t){var e,i=c(t.key);"method"===t.kind?e={value:t.value,writable:!0,configurable:!0,enumerable:!1}:"get"===t.kind?e={get:t.value,configurable:!0,enumerable:!1}:"set"===t.kind?e={set:t.value,configurable:!0,enumerable:!1}:"field"===t.kind&&(e={configurable:!0,writable:!0,enumerable:!0});var r={kind:"field"===t.kind?"field":"method",key:i,placement:t.static?"static":"field"===t.kind?"own":"prototype",descriptor:e};return t.decorators&&(r.decorators=t.decorators),"field"===t.kind&&(r.initializer=t.value),r}function o(t,e){void 0!==t.descriptor.get?e.descriptor.get=t.descriptor.get:e.descriptor.set=t.descriptor.set}function a(t){return t.decorators&&t.decorators.length}function n(t){return void 0!==t&&!(void 0===t.value&&void 0===t.writable)}function l(t,e){var i=t[e];if(void 0!==i&&"function"!=typeof i)throw new TypeError("Expected '"+e+"' to be a function");return i}function c(t){var e=function(t,e){if("object"!=typeof t||null===t)return t;var i=t[Symbol.toPrimitive];if(void 0!==i){var r=i.call(t,e||"default");if("object"!=typeof r)return r;throw new TypeError("@@toPrimitive must return a primitive value.")}return("string"===e?String:Number)(t)}(t,"string");return"symbol"==typeof e?e:String(e)}let p=function(t,e,i,r){var p=function(){var t={elementsDefinitionOrder:[["method"],["field"]],initializeInstanceElements:function(t,e){["method","field"].forEach(function(i){e.forEach(function(e){e.kind===i&&"own"===e.placement&&this.defineClassElement(t,e)},this)},this)},initializeClassElements:function(t,e){var i=t.prototype;["method","field"].forEach(function(r){e.forEach(function(e){var s=e.placement;if(e.kind===r&&("static"===s||"prototype"===s)){var o="static"===s?t:i;this.defineClassElement(o,e)}},this)},this)},defineClassElement:function(t,e){var i=e.descriptor;if("field"===e.kind){var r=e.initializer;i={enumerable:i.enumerable,writable:i.writable,configurable:i.configurable,value:void 0===r?void 0:r.call(t)}}Object.defineProperty(t,e.key,i)},decorateClass:function(t,e){var i=[],r=[],s={static:[],prototype:[],own:[]};if(t.forEach(function(t){this.addElementPlacement(t,s)},this),t.forEach(function(t){if(!a(t))return i.push(t);var e=this.decorateElement(t,s);i.push(e.element),i.push.apply(i,e.extras),r.push.apply(r,e.finishers)},this),!e)return{elements:i,finishers:r};var o=this.decorateConstructor(i,e);return r.push.apply(r,o.finishers),o.finishers=r,o},addElementPlacement:function(t,e,i){var r=e[t.placement];if(!i&&-1!==r.indexOf(t.key))throw new TypeError("Duplicated element ("+t.key+")");r.push(t.key)},decorateElement:function(t,e){for(var i=[],r=[],s=t.decorators,o=s.length-1;o>=0;o--){var a=e[t.placement];a.splice(a.indexOf(t.key),1);var n=this.fromElementDescriptor(t),l=this.toElementFinisherExtras((0,s[o])(n)||n);t=l.element,this.addElementPlacement(t,e),l.finisher&&r.push(l.finisher);var c=l.extras;if(c){for(var p=0;p<c.length;p++)this.addElementPlacement(c[p],e);i.push.apply(i,c)}}return{element:t,finishers:r,extras:i}},decorateConstructor:function(t,e){for(var i=[],r=e.length-1;r>=0;r--){var s=this.fromClassDescriptor(t),o=this.toClassDescriptor((0,e[r])(s)||s);if(void 0!==o.finisher&&i.push(o.finisher),void 0!==o.elements){t=o.elements;for(var a=0;a<t.length-1;a++)for(var n=a+1;n<t.length;n++)if(t[a].key===t[n].key&&t[a].placement===t[n].placement)throw new TypeError("Duplicated element ("+t[a].key+")")}}return{elements:t,finishers:i}},fromElementDescriptor:function(t){var e={kind:t.kind,key:t.key,placement:t.placement,descriptor:t.descriptor};return Object.defineProperty(e,Symbol.toStringTag,{value:"Descriptor",configurable:!0}),"field"===t.kind&&(e.initializer=t.initializer),e},toElementDescriptors:function(t){var e;if(void 0!==t)return(e=t,function(t){if(Array.isArray(t))return t}(e)||function(t){if(Symbol.iterator in Object(t)||"[object Arguments]"===Object.prototype.toString.call(t))return Array.from(t)}(e)||function(){throw new TypeError("Invalid attempt to destructure non-iterable instance")}()).map(function(t){var e=this.toElementDescriptor(t);return this.disallowProperty(t,"finisher","An element descriptor"),this.disallowProperty(t,"extras","An element descriptor"),e},this)},toElementDescriptor:function(t){var e=String(t.kind);if("method"!==e&&"field"!==e)throw new TypeError('An element descriptor\'s .kind property must be either "method" or "field", but a decorator created an element descriptor with .kind "'+e+'"');var i=c(t.key),r=String(t.placement);if("static"!==r&&"prototype"!==r&&"own"!==r)throw new TypeError('An element descriptor\'s .placement property must be one of "static", "prototype" or "own", but a decorator created an element descriptor with .placement "'+r+'"');var s=t.descriptor;this.disallowProperty(t,"elements","An element descriptor");var o={kind:e,key:i,placement:r,descriptor:Object.assign({},s)};return"field"!==e?this.disallowProperty(t,"initializer","A method descriptor"):(this.disallowProperty(s,"get","The property descriptor of a field descriptor"),this.disallowProperty(s,"set","The property descriptor of a field descriptor"),this.disallowProperty(s,"value","The property descriptor of a field descriptor"),o.initializer=t.initializer),o},toElementFinisherExtras:function(t){var e=this.toElementDescriptor(t),i=l(t,"finisher"),r=this.toElementDescriptors(t.extras);return{element:e,finisher:i,extras:r}},fromClassDescriptor:function(t){var e={kind:"class",elements:t.map(this.fromElementDescriptor,this)};return Object.defineProperty(e,Symbol.toStringTag,{value:"Descriptor",configurable:!0}),e},toClassDescriptor:function(t){var e=String(t.kind);if("class"!==e)throw new TypeError('A class descriptor\'s .kind property must be "class", but a decorator created a class descriptor with .kind "'+e+'"');this.disallowProperty(t,"key","A class descriptor"),this.disallowProperty(t,"placement","A class descriptor"),this.disallowProperty(t,"descriptor","A class descriptor"),this.disallowProperty(t,"initializer","A class descriptor"),this.disallowProperty(t,"extras","A class descriptor");var i=l(t,"finisher"),r=this.toElementDescriptors(t.elements);return{elements:r,finisher:i}},runClassFinishers:function(t,e){for(var i=0;i<e.length;i++){var r=(0,e[i])(t);if(void 0!==r){if("function"!=typeof r)throw new TypeError("Finishers must return a constructor.");t=r}}return t},disallowProperty:function(t,e,i){if(void 0!==t[e])throw new TypeError(i+" can't have a ."+e+" property.")}};return t}();if(r)for(var u=0;u<r.length;u++)p=r[u](p);var d=e(function(t){p.initializeInstanceElements(t,m.elements)},i),m=p.decorateClass(function(t){for(var e=[],i=function(t){return"method"===t.kind&&t.key===l.key&&t.placement===l.placement},r=0;r<t.length;r++){var s,l=t[r];if("method"===l.kind&&(s=e.find(i)))if(n(l.descriptor)||n(s.descriptor)){if(a(l)||a(s))throw new ReferenceError("Duplicated methods ("+l.key+") can't be decorated.");s.descriptor=l.descriptor}else{if(a(l)){if(a(s))throw new ReferenceError("Decorators can't be placed on different accessors with for the same property ("+l.key+").");s.decorators=l.decorators}o(l,s)}else e.push(l)}return e}(d.d.map(s)),t);return p.initializeClassElements(d.F,m.elements),p.runClassFinishers(d.F,m.finishers)}(null,function(t,e){return{F:class extends e{constructor(...e){super(...e),t(this)}},d:[{kind:"field",decorators:[Object(r.g)()],key:"header",value:void 0},{kind:"get",static:!0,key:"styles",value:function(){return r.c`
      :host {
        background: var(
          --ha-card-background,
          var(--paper-card-background-color, white)
        );
        border-radius: var(--ha-card-border-radius, 2px);
        box-shadow: var(
          --ha-card-box-shadow,
          0 2px 2px 0 rgba(0, 0, 0, 0.14),
          0 1px 5px 0 rgba(0, 0, 0, 0.12),
          0 3px 1px -2px rgba(0, 0, 0, 0.2)
        );
        color: var(--primary-text-color);
        display: block;
        transition: all 0.3s ease-out;
        position: relative;
      }

      .card-header,
      :host ::slotted(.card-header) {
        color: var(--ha-card-header-color, --primary-text-color);
        font-family: var(--ha-card-header-font-family, inherit);
        font-size: var(--ha-card-header-font-size, 24px);
        letter-spacing: -0.012em;
        line-height: 32px;
        padding: 24px 16px 16px;
        display: block;
      }

      :host ::slotted(.card-content:not(:first-child)),
      slot:not(:first-child)::slotted(.card-content) {
        padding-top: 0px;
        margin-top: -8px;
      }

      :host ::slotted(.card-content) {
        padding: 16px;
      }

      :host ::slotted(.card-actions) {
        border-top: 1px solid #e8e8e8;
        padding: 5px 16px;
      }
    `}},{kind:"method",key:"render",value:function(){return r.f`
      ${this.header?r.f`
            <div class="card-header">${this.header}</div>
          `:r.f``}
      <slot></slot>
    `}}]}},r.a);customElements.define("ha-card",p)},192:function(t,e,i){"use strict";i.d(e,"a",function(){return o});var r=i(8),s=i(15);const o=Object(r.a)(t=>(class extends t{fire(t,e,i){return i=i||{},Object(s.a)(i.node||this,t,e,i)}}))},195:function(t,e,i){"use strict";i.d(e,"a",function(){return s});var r=i(127);const s=t=>Object(r.a)(t.entity_id)},209:function(t,e,i){"use strict";var r=i(3),s=i(25);i(97);customElements.define("ha-config-section",class extends s.a{static get template(){return r.a`
      <style include="iron-flex ha-style">
        .content {
          padding: 28px 20px 0;
          max-width: 1040px;
          margin: 0 auto;
        }

        .header {
          @apply --paper-font-display1;
          opacity: var(--dark-primary-opacity);
        }

        .together {
          margin-top: 32px;
        }

        .intro {
          @apply --paper-font-subhead;
          width: 100%;
          max-width: 400px;
          margin-right: 40px;
          opacity: var(--dark-primary-opacity);
        }

        .panel {
          margin-top: -24px;
        }

        .panel ::slotted(*) {
          margin-top: 24px;
          display: block;
        }

        .narrow.content {
          max-width: 640px;
        }
        .narrow .together {
          margin-top: 20px;
        }
        .narrow .header {
          @apply --paper-font-headline;
        }
        .narrow .intro {
          font-size: 14px;
          padding-bottom: 20px;
          margin-right: 0;
          max-width: 500px;
        }
      </style>
      <div class$="[[computeContentClasses(isWide)]]">
        <div class="header"><slot name="header"></slot></div>
        <div class$="[[computeClasses(isWide)]]">
          <div class="intro"><slot name="introduction"></slot></div>
          <div class="panel flex-auto"><slot></slot></div>
        </div>
      </div>
    `}static get properties(){return{hass:{type:Object},narrow:{type:Boolean},isWide:{type:Boolean,value:!1}}}computeContentClasses(t){return t?"content ":"content narrow"}computeClasses(t){return"together layout "+(t?"horizontal":"vertical narrow")}})},404:function(t,e,i){"use strict";const r={DOMAIN_DEVICE_CLASS:{binary_sensor:["battery","cold","connectivity","door","garage_door","gas","heat","light","lock","moisture","motion","moving","occupancy","opening","plug","power","presence","problem","safety","smoke","sound","vibration","window"],cover:["awning","blind","curtain","damper","door","garage","shade","shutter","window"],sensor:["battery","humidity","illuminance","temperature","pressure","power","signal_strength","timestamp"],switch:["switch","outlet"]},UNKNOWN_TYPE:"json",ADD_TYPE:"key-value",TYPE_TO_TAG:{string:"ha-customize-string",json:"ha-customize-string",icon:"ha-customize-icon",boolean:"ha-customize-boolean",array:"ha-customize-array","key-value":"ha-customize-key-value"}};r.LOGIC_STATE_ATTRIBUTES=r.LOGIC_STATE_ATTRIBUTES||{entity_picture:void 0,friendly_name:{type:"string",description:"Name"},icon:{type:"icon"},emulated_hue:{type:"boolean",domains:["emulated_hue"]},emulated_hue_name:{type:"string",domains:["emulated_hue"]},haaska_hidden:void 0,haaska_name:void 0,homebridge_hidden:{type:"boolean"},homebridge_name:{type:"string"},supported_features:void 0,attribution:void 0,custom_ui_more_info:{type:"string"},custom_ui_state_card:{type:"string"},device_class:{type:"array",options:r.DOMAIN_DEVICE_CLASS,description:"Device class",domains:["binary_sensor","cover","sensor","switch"]},hidden:{type:"boolean",description:"Hide from UI"},assumed_state:{type:"boolean",domains:["switch","light","cover","climate","fan","group","water_heater"]},initial_state:{type:"string",domains:["automation"]},unit_of_measurement:{type:"string"}},e.a=r},459:function(t,e){const i=document.createElement("template");i.setAttribute("style","display: none;"),i.innerHTML='<dom-module id="ha-form-style">\n  <template>\n    <style>\n      .form-group {\n        @apply --layout-horizontal;\n        @apply --layout-center;\n        padding: 8px 16px;\n      }\n\n      .form-group label {\n        @apply --layout-flex-2;\n      }\n\n      .form-group .form-control {\n        @apply --layout-flex;\n      }\n\n      .form-group.vertical {\n        @apply --layout-vertical;\n        @apply --layout-start;\n      }\n\n      paper-dropdown-menu.form-control {\n        margin: -9px 0;\n      }\n    </style>\n  </template>\n</dom-module>',document.head.appendChild(i.content)},460:function(t,e,i){"use strict";i.d(e,"a",function(){return s});var r=i(124);const s=(t,e)=>{const i=Object(r.a)(t),s=Object(r.a)(e);return i<s?-1:i>s?1:0}},747:function(t,e,i){"use strict";i.r(e);i(217),i(239),i(159),i(114);var r=i(3),s=i(25),o=(i(97),i(139),i(209),i(87),i(155),i(152),i(154),i(202),i(191),i(124));customElements.define("ha-entity-config",class extends s.a{static get template(){return r.a`
      <style include="iron-flex ha-style">
        ha-card {
          direction: ltr;
        }

        .device-picker {
          @apply --layout-horizontal;
          padding-bottom: 24px;
        }

        .form-placeholder {
          @apply --layout-vertical;
          @apply --layout-center-center;
          height: 96px;
        }

        [hidden]: {
          display: none;
        }

        .card-actions {
          @apply --layout-horizontal;
          @apply --layout-justified;
        }
      </style>
      <ha-card>
        <div class="card-content">
          <div class="device-picker">
            <paper-dropdown-menu
              label="[[label]]"
              class="flex"
              disabled="[[!entities.length]]"
            >
              <paper-listbox
                slot="dropdown-content"
                selected="{{selectedEntity}}"
              >
                <template is="dom-repeat" items="[[entities]]" as="state">
                  <paper-item>[[computeSelectCaption(state)]]</paper-item>
                </template>
              </paper-listbox>
            </paper-dropdown-menu>
          </div>

          <div class="form-container">
            <template is="dom-if" if="[[computeShowPlaceholder(formState)]]">
              <div class="form-placeholder">
                <template is="dom-if" if="[[computeShowNoDevices(formState)]]">
                  No entities found! :-(
                </template>

                <template is="dom-if" if="[[computeShowSpinner(formState)]]">
                  <paper-spinner active="" alt="[[formState]]"></paper-spinner>
                  [[formState]]
                </template>
              </div>
            </template>

            <div hidden$="[[!computeShowForm(formState)]]" id="form"></div>
          </div>
        </div>
        <div class="card-actions">
          <mwc-button
            on-click="saveEntity"
            disabled="[[computeShowPlaceholder(formState)]]"
            >SAVE</mwc-button
          >
          <template is="dom-if" if="[[allowDelete]]">
            <mwc-button
              class="warning"
              on-click="deleteEntity"
              disabled="[[computeShowPlaceholder(formState)]]"
              >DELETE</mwc-button
            >
          </template>
        </div>
      </ha-card>
    `}static get properties(){return{hass:{type:Object,observer:"hassChanged"},label:{type:String,value:"Device"},entities:{type:Array,observer:"entitiesChanged"},allowDelete:{type:Boolean,value:!1},selectedEntity:{type:Number,value:-1,observer:"entityChanged"},formState:{type:String,value:"no-devices"},config:{type:Object}}}connectedCallback(){super.connectedCallback(),this.formEl=document.createElement(this.config.component),this.formEl.hass=this.hass,this.$.form.appendChild(this.formEl),this.entityChanged(this.selectedEntity)}computeSelectCaption(t){return this.config.computeSelectCaption?this.config.computeSelectCaption(t):Object(o.a)(t)}computeShowNoDevices(t){return"no-devices"===t}computeShowSpinner(t){return"loading"===t||"saving"===t}computeShowPlaceholder(t){return"editing"!==t}computeShowForm(t){return"editing"===t}hassChanged(t){this.formEl&&(this.formEl.hass=t)}entitiesChanged(t,e){if(0!==t.length)if(e){var i=e[this.selectedEntity].entity_id,r=t.findIndex(function(t){return t.entity_id===i});-1===r?this.selectedEntity=0:r!==this.selectedEntity&&(this.selectedEntity=r)}else this.selectedEntity=0;else this.formState="no-devices"}entityChanged(t){if(this.entities&&this.formEl){var e=this.entities[t];if(e){this.formState="loading";var i=this;this.formEl.loadEntity(e).then(function(){i.formState="editing"})}}}saveEntity(){this.formState="saving";var t=this;this.formEl.saveEntity().then(function(){t.formState="editing"})}});var a=i(190),n=i(404),l=i(27),c=(i(459),i(192));customElements.define("ha-customize-array",class extends(Object(c.a)(s.a)){static get template(){return r.a`
      <style>
        paper-dropdown-menu {
          margin: -9px 0;
        }
      </style>
      <paper-dropdown-menu
        label="[[item.description]]"
        disabled="[[item.secondary]]"
        selected-item-label="{{item.value}}"
        dynamic-align=""
      >
        <paper-listbox
          slot="dropdown-content"
          selected="[[computeSelected(item)]]"
        >
          <template is="dom-repeat" items="[[getOptions(item)]]" as="option">
            <paper-item>[[option]]</paper-item>
          </template>
        </paper-listbox>
      </paper-dropdown-menu>
    `}static get properties(){return{item:{type:Object,notifies:!0}}}getOptions(t){const e=t.domain||"*",i=t.options[e]||t.options["*"];return i?i.sort():(this.item.type="string",this.fire("item-changed"),[])}computeSelected(t){return this.getOptions(t).indexOf(t.value)}});i(166);customElements.define("ha-customize-boolean",class extends s.a{static get template(){return r.a`
      <paper-checkbox disabled="[[item.secondary]]" checked="{{item.value}}">
        [[item.description]]
      </paper-checkbox>
    `}static get properties(){return{item:{type:Object,notifies:!0}}}});i(115),i(68);customElements.define("ha-customize-icon",class extends s.a{static get template(){return r.a`
      <style>
        :host {
          @apply --layout-horizontal;
        }
        .icon-image {
          border: 1px solid grey;
          padding: 8px;
          margin-right: 20px;
          margin-top: 10px;
        }
      </style>
      <iron-icon class="icon-image" icon="[[item.value]]"></iron-icon>
      <paper-input
        disabled="[[item.secondary]]"
        label="icon"
        value="{{item.value}}"
      >
      </paper-input>
    `}static get properties(){return{item:{type:Object,notifies:!0}}}});customElements.define("ha-customize-key-value",class extends s.a{static get template(){return r.a`
      <style>
        :host {
          @apply --layout-horizontal;
        }
        paper-input {
          @apply --layout-flex;
        }
        .key {
          padding-right: 20px;
        }
      </style>
      <paper-input
        disabled="[[item.secondary]]"
        class="key"
        label="Attribute name"
        value="{{item.attribute}}"
      >
      </paper-input>
      <paper-input
        disabled="[[item.secondary]]"
        label="Attribute value"
        value="{{item.value}}"
      >
      </paper-input>
    `}static get properties(){return{item:{type:Object,notifies:!0}}}});customElements.define("ha-customize-string",class extends s.a{static get template(){return r.a`
      <paper-input
        disabled="[[item.secondary]]"
        label="[[getLabel(item)]]"
        value="{{item.value}}"
      >
      </paper-input>
    `}static get properties(){return{item:{type:Object,notifies:!0}}}getLabel(t){return t.description+("json"===t.type?" (JSON formatted)":"")}});customElements.define("ha-customize-attribute",class extends s.a{static get template(){return r.a`
      <style include="ha-form-style">
        :host {
          display: block;
          position: relative;
          padding-right: 40px;
        }

        .button {
          position: absolute;
          margin-top: -20px;
          top: 50%;
          right: 0;
        }
      </style>
      <div id="wrapper" class="form-group"></div>
      <paper-icon-button
        class="button"
        icon="[[getIcon(item.secondary)]]"
        on-click="tapButton"
      ></paper-icon-button>
    `}static get properties(){return{item:{type:Object,notify:!0,observer:"itemObserver"}}}tapButton(){this.item.secondary?this.item=Object.assign({},this.item,{secondary:!1}):this.item=Object.assign({},this.item,{closed:!0})}getIcon(t){return t?"hass:pencil":"hass:close"}itemObserver(t){const e=this.$.wrapper,i=n.a.TYPE_TO_TAG[t.type].toUpperCase();let r;e.lastChild&&e.lastChild.tagName===i?r=e.lastChild:(e.lastChild&&e.removeChild(e.lastChild),this.$.child=r=document.createElement(i.toLowerCase()),r.className="form-control",r.addEventListener("item-changed",()=>{this.item=Object.assign({},r.item)})),r.setProperties({item:this.item}),null===r.parentNode&&e.appendChild(r)}});customElements.define("ha-form-customize-attributes",class extends(Object(l.a)(s.a)){static get template(){return r.a`
      <style>
        [hidden] {
          display: none;
        }
      </style>
      <template is="dom-repeat" items="{{attributes}}" mutable-data="">
        <ha-customize-attribute item="{{item}}" hidden$="[[item.closed]]">
        </ha-customize-attribute>
      </template>
    `}static get properties(){return{attributes:{type:Array,notify:!0}}}});var p=i(195);customElements.define("ha-form-customize",class extends(Object(a.a)(s.a)){static get template(){return r.a`
      <style include="iron-flex ha-style ha-form-style">
        .warning {
          color: red;
        }

        .attributes-text {
          padding-left: 20px;
        }
      </style>
      <template
        is="dom-if"
        if="[[computeShowWarning(localConfig, globalConfig)]]"
      >
        <div class="warning">
          [[localize('ui.panel.config.customize.warning.include_sentence')]]
          <a
            href="https://www.home-assistant.io/docs/configuration/customizing-devices/#customization-using-the-ui"
            target="_blank"
            >[[localize('ui.panel.config.customize.warning.include_link')]]</a
          >.<br />
          [[localize('ui.panel.config.customize.warning.not_applied')]]
        </div>
      </template>
      <template is="dom-if" if="[[hasLocalAttributes]]">
        <h4 class="attributes-text">
          [[localize('ui.panel.config.customize.attributes_customize')]]<br />
        </h4>
        <ha-form-customize-attributes
          attributes="{{localAttributes}}"
        ></ha-form-customize-attributes>
      </template>
      <template is="dom-if" if="[[hasGlobalAttributes]]">
        <h4 class="attributes-text">
          [[localize('ui.panel.config.customize.attributes_outside')]]<br />
          [[localize('ui.panel.config.customize.different_include')]]
        </h4>
        <ha-form-customize-attributes
          attributes="{{globalAttributes}}"
        ></ha-form-customize-attributes>
      </template>
      <template is="dom-if" if="[[hasExistingAttributes]]">
        <h4 class="attributes-text">
          [[localize('ui.panel.config.customize.attributes_set')]]<br />
          [[localize('ui.panel.config.customize.attributes_override')]]
        </h4>
        <ha-form-customize-attributes
          attributes="{{existingAttributes}}"
        ></ha-form-customize-attributes>
      </template>
      <template is="dom-if" if="[[hasNewAttributes]]">
        <h4 class="attributes-text">
          [[localize('ui.panel.config.customize.attributes_not_set')]]
        </h4>
        <ha-form-customize-attributes
          attributes="{{newAttributes}}"
        ></ha-form-customize-attributes>
      </template>
      <div class="form-group">
        <paper-dropdown-menu
          label="[[localize('ui.panel.config.customize.pick_attribute')]]"
          class="flex"
          dynamic-align=""
        >
          <paper-listbox
            slot="dropdown-content"
            selected="{{selectedNewAttribute}}"
          >
            <template
              is="dom-repeat"
              items="[[newAttributesOptions]]"
              as="option"
            >
              <paper-item>[[option]]</paper-item>
            </template>
          </paper-listbox>
        </paper-dropdown-menu>
      </div>
    `}static get properties(){return{hass:{type:Object},entity:Object,localAttributes:{type:Array,computed:"computeLocalAttributes(localConfig)"},hasLocalAttributes:Boolean,globalAttributes:{type:Array,computed:"computeGlobalAttributes(localConfig, globalConfig)"},hasGlobalAttributes:Boolean,existingAttributes:{type:Array,computed:"computeExistingAttributes(localConfig, globalConfig, entity)"},hasExistingAttributes:Boolean,newAttributes:{type:Array,value:[]},hasNewAttributes:Boolean,newAttributesOptions:Array,selectedNewAttribute:{type:Number,value:-1,observer:"selectedNewAttributeObserver"},localConfig:Object,globalConfig:Object}}static get observers(){return["attributesObserver(localAttributes.*, globalAttributes.*, existingAttributes.*, newAttributes.*)"]}_initOpenObject(t,e,i,r){return Object.assign({attribute:t,value:e,closed:!1,domain:Object(p.a)(this.entity),secondary:i,description:t},r)}loadEntity(t){return this.entity=t,this.hass.callApi("GET","config/customize/config/"+t.entity_id).then(t=>{this.localConfig=t.local,this.globalConfig=t.global,this.newAttributes=[]})}saveEntity(){const t={};this.localAttributes.concat(this.globalAttributes,this.existingAttributes,this.newAttributes).forEach(e=>{if(e.closed||e.secondary||!e.attribute||!e.value)return;const i="json"===e.type?JSON.parse(e.value):e.value;i&&(t[e.attribute]=i)});const e=this.entity.entity_id;return this.hass.callApi("POST","config/customize/config/"+e,t)}_computeSingleAttribute(t,e,i){const r=n.a.LOGIC_STATE_ATTRIBUTES[t]||{type:n.a.UNKNOWN_TYPE};return this._initOpenObject(t,"json"===r.type?JSON.stringify(e):e,i,r)}_computeAttributes(t,e,i){return e.map(e=>this._computeSingleAttribute(e,t[e],i))}computeLocalAttributes(t){if(!t)return[];const e=Object.keys(t);return this._computeAttributes(t,e,!1)}computeGlobalAttributes(t,e){if(!t||!e)return[];const i=Object.keys(t),r=Object.keys(e).filter(t=>!i.includes(t));return this._computeAttributes(e,r,!0)}computeExistingAttributes(t,e,i){if(!t||!e||!i)return[];const r=Object.keys(t),s=Object.keys(e),o=Object.keys(i.attributes).filter(t=>!r.includes(t)&&!s.includes(t));return this._computeAttributes(i.attributes,o,!0)}computeShowWarning(t,e){return!(!t||!e)&&Object.keys(t).some(i=>JSON.stringify(e[i])!==JSON.stringify(t[i]))}filterFromAttributes(t){return e=>!t||t.every(t=>t.attribute!==e||t.closed)}getNewAttributesOptions(t,e,i,r){return Object.keys(n.a.LOGIC_STATE_ATTRIBUTES).filter(t=>{const e=n.a.LOGIC_STATE_ATTRIBUTES[t];return e&&(!e.domains||!this.entity||e.domains.includes(Object(p.a)(this.entity)))}).filter(this.filterFromAttributes(t)).filter(this.filterFromAttributes(e)).filter(this.filterFromAttributes(i)).filter(this.filterFromAttributes(r)).sort().concat("Other")}selectedNewAttributeObserver(t){if(t<0)return;const e=this.newAttributesOptions[t];if(t===this.newAttributesOptions.length-1){const t=this._initOpenObject("","",!1,{type:n.a.ADD_TYPE});return this.push("newAttributes",t),void(this.selectedNewAttribute=-1)}let i=this.localAttributes.findIndex(t=>t.attribute===e);if(i>=0)return this.set("localAttributes."+i+".closed",!1),void(this.selectedNewAttribute=-1);if((i=this.globalAttributes.findIndex(t=>t.attribute===e))>=0)return this.set("globalAttributes."+i+".closed",!1),void(this.selectedNewAttribute=-1);if((i=this.existingAttributes.findIndex(t=>t.attribute===e))>=0)return this.set("existingAttributes."+i+".closed",!1),void(this.selectedNewAttribute=-1);if((i=this.newAttributes.findIndex(t=>t.attribute===e))>=0)return this.set("newAttributes."+i+".closed",!1),void(this.selectedNewAttribute=-1);const r=this._computeSingleAttribute(e,"",!1);this.push("newAttributes",r),this.selectedNewAttribute=-1}attributesObserver(){this.hasLocalAttributes=this.localAttributes&&this.localAttributes.some(t=>!t.closed),this.hasGlobalAttributes=this.globalAttributes&&this.globalAttributes.some(t=>!t.closed),this.hasExistingAttributes=this.existingAttributes&&this.existingAttributes.some(t=>!t.closed),this.hasNewAttributes=this.newAttributes&&this.newAttributes.some(t=>!t.closed),this.newAttributesOptions=this.getNewAttributesOptions(this.localAttributes,this.globalAttributes,this.existingAttributes,this.newAttributes)}});var u=i(460);customElements.define("ha-config-customize",class extends(Object(a.a)(s.a)){static get template(){return r.a`
      <style include="ha-style"></style>

      <app-header-layout has-scrolling-region="">
        <app-header slot="header" fixed="">
          <app-toolbar>
            <ha-paper-icon-button-arrow-prev
              on-click="_backTapped"
            ></ha-paper-icon-button-arrow-prev>
            <div main-title="">
              [[localize('ui.panel.config.customize.caption')]]
            </div>
          </app-toolbar>
        </app-header>

        <div class$="[[computeClasses(isWide)]]">
          <ha-config-section is-wide="[[isWide]]">
            <span slot="header">
              [[localize('ui.panel.config.customize.picker.header')]]
            </span>
            <span slot="introduction">
              [[localize('ui.panel.config.customize.picker.introduction')]]
            </span>
            <ha-entity-config
              hass="[[hass]]"
              label="Entity"
              entities="[[entities]]"
              config="[[entityConfig]]"
            >
            </ha-entity-config>
          </ha-config-section>
        </div>
      </app-header-layout>
    `}static get properties(){return{hass:Object,isWide:Boolean,entities:{type:Array,computed:"computeEntities(hass)"},entityConfig:{type:Object,value:{component:"ha-form-customize",computeSelectCaption:t=>Object(o.a)(t)+" ("+Object(p.a)(t)+")"}}}}computeClasses(t){return t?"content":"content narrow"}_backTapped(){history.back()}computeEntities(t){return Object.keys(t.states).map(e=>t.states[e]).sort(u.a)}})}}]);
//# sourceMappingURL=chunk.abb0099be04f84106a56.js.map