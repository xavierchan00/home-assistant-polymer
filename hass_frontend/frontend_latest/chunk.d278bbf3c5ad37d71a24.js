/*! For license information please see chunk.d278bbf3c5ad37d71a24.js.LICENSE */
(self.webpackJsonp=self.webpackJsonp||[]).push([[43],{202:function(e,i,s){"use strict";s(4),s(71),s(164);var a=s(5),t=s(3),l=s(131);const o=t.a`
  <style include="paper-spinner-styles"></style>

  <div id="spinnerContainer" class-name="[[__computeContainerClasses(active, __coolingDown)]]" on-animationend="__reset" on-webkit-animation-end="__reset">
    <div class="spinner-layer layer-1">
      <div class="circle-clipper left">
        <div class="circle"></div>
      </div>
      <div class="circle-clipper right">
        <div class="circle"></div>
      </div>
    </div>

    <div class="spinner-layer layer-2">
      <div class="circle-clipper left">
        <div class="circle"></div>
      </div>
      <div class="circle-clipper right">
        <div class="circle"></div>
      </div>
    </div>

    <div class="spinner-layer layer-3">
      <div class="circle-clipper left">
        <div class="circle"></div>
      </div>
      <div class="circle-clipper right">
        <div class="circle"></div>
      </div>
    </div>

    <div class="spinner-layer layer-4">
      <div class="circle-clipper left">
        <div class="circle"></div>
      </div>
      <div class="circle-clipper right">
        <div class="circle"></div>
      </div>
    </div>
  </div>
`;o.setAttribute("strip-whitespace",""),Object(a.a)({_template:o,is:"paper-spinner",behaviors:[l.a]})},205:function(e,i,s){"use strict";s(160);var a=s(75),t=s(1),l=s(99);const o={getTabbableNodes:function(e){var i=[];return this._collectTabbableNodes(e,i)?l.a._sortByTabIndex(i):i},_collectTabbableNodes:function(e,i){if(e.nodeType!==Node.ELEMENT_NODE||!l.a._isVisible(e))return!1;var s,a=e,o=l.a._normalizedTabIndex(a),r=o>0;o>=0&&i.push(a),s="content"===a.localName||"slot"===a.localName?Object(t.a)(a).getDistributedNodes():Object(t.a)(a.shadowRoot||a.root||a).children;for(var c=0;c<s.length;c++)r=this._collectTabbableNodes(s[c],i)||r;return r}},r=customElements.get("paper-dialog"),c={get _focusableNodes(){return o.getTabbableNodes(this)}};customElements.define("ha-paper-dialog",class extends(Object(a.b)([c],r)){})},674:function(e,i,s){"use strict";s.r(i);s(87),s(202);var a=s(3),t=s(25),l=(s(97),s(205),s(190));customElements.define("ha-dialog-show-audio-message",class extends(Object(l.a)(t.a)){static get template(){return a.a`
      <style include="ha-style-dialog">
        .error {
          color: red;
        }
        @media all and (max-width: 500px) {
          ha-paper-dialog {
            margin: 0;
            width: 100%;
            max-height: calc(100% - 64px);

            position: fixed !important;
            bottom: 0px;
            left: 0px;
            right: 0px;
            overflow: scroll;
            border-bottom-left-radius: 0px;
            border-bottom-right-radius: 0px;
          }
        }

        ha-paper-dialog {
          border-radius: 2px;
        }
        ha-paper-dialog p {
          color: var(--secondary-text-color);
        }

        .icon {
          float: right;
        }
      </style>
      <ha-paper-dialog
        id="mp3dialog"
        with-backdrop
        opened="{{_opened}}"
        on-opened-changed="_openedChanged"
      >
        <h2>
          [[localize('ui.panel.mailbox.playback_title')]]
          <div class="icon">
            <template is="dom-if" if="[[_loading]]">
              <paper-spinner active></paper-spinner>
            </template>
            <paper-icon-button
              id="delicon"
              on-click="openDeleteDialog"
              icon="hass:delete"
            ></paper-icon-button>
          </div>
        </h2>
        <div id="transcribe"></div>
        <div>
          <template is="dom-if" if="[[_errorMsg]]">
            <div class="error">[[_errorMsg]]</div>
          </template>
          <audio id="mp3" preload="none" controls>
            <source id="mp3src" src="" type="audio/mpeg" />
          </audio>
        </div>
      </ha-paper-dialog>
    `}static get properties(){return{hass:Object,_currentMessage:Object,_errorMsg:String,_loading:{type:Boolean,value:!1},_opened:{type:Boolean,value:!1}}}showDialog({hass:e,message:i}){this.hass=e,this._errorMsg=null,this._currentMessage=i,this._opened=!0,this.$.transcribe.innerText=i.message;const s=i.platform,a=this.$.mp3;if(s.has_media){a.style.display="",this._showLoading(!0),a.src=null;const e=`/api/mailbox/media/${s.name}/${i.sha}`;this.hass.fetchWithAuth(e).then(e=>e.ok?e.blob():Promise.reject({status:e.status,statusText:e.statusText})).then(e=>{this._showLoading(!1),a.src=window.URL.createObjectURL(e),a.play()}).catch(e=>{this._showLoading(!1),this._errorMsg=`Error loading audio: ${e.statusText}`})}else a.style.display="none",this._showLoading(!1)}openDeleteDialog(){confirm(this.localize("ui.panel.mailbox.delete_prompt"))&&this.deleteSelected()}deleteSelected(){const e=this._currentMessage;this.hass.callApi("DELETE",`mailbox/delete/${e.platform.name}/${e.sha}`),this._dialogDone()}_dialogDone(){this.$.mp3.pause(),this.setProperties({_currentMessage:null,_errorMsg:null,_loading:!1,_opened:!1})}_openedChanged(e){e.detail.value||this._dialogDone()}_showLoading(e){const i=this.$.delicon;if(e)this._loading=!0,i.style.display="none";else{const e=this._currentMessage.platform;this._loading=!1,i.style.display=e.can_delete?"":"none"}}})}}]);
//# sourceMappingURL=chunk.d278bbf3c5ad37d71a24.js.map